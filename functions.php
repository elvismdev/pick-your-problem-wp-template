<?php
add_action( 'wp_enqueue_scripts', 'theme_enqueue_styles' );
function theme_enqueue_styles() {
	wp_enqueue_style( 'parent-style', get_template_directory_uri() . '/style.css' );

    // Adding Bootstrap.
	wp_enqueue_style( 'bootstrap', get_stylesheet_directory_uri() . '/bower_components/bootstrap/dist/css/bootstrap.min.css' );

	// wp_enqueue_style( 'child-style',
	// 	get_stylesheet_directory_uri() . '/style.css',
	// 	array('parent-style')
	// 	);
}

add_filter( 'twentysixteen_custom_header_args', 'wpb_new_header_size');
//add your parameters in - https://codex.wordpress.org/Custom_Headers
function wpb_new_header_size($array) {
	$array = array (
		'default-text-color'     => $default_text_color,
		'width'                  => 2560,
		'height'                 => 540,
		'flex-height'            => true,
		'wp-head-callback'       => 'twentysixteen_header_style',
		);
	return $array;
}

remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_excerpt', 'wpautop' );